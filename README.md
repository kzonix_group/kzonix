# Kzonix [WIP]

[![Build Status](http://limpid.kzonix.com.ua:9000/buildStatus/icon?job=kzonix%2Fmaster&style=flat-square)](http://limpid.kzonix.com.ua:9000/job/kzonix/job/master/)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=kzonix_kzonix&metric=alert_status)](https://sonarcloud.io/dashboard?id=kzonix_kzonix)
[![Total alerts](https://img.shields.io/lgtm/alerts/g/kzonix/kzonix.svg?logo=lgtm&logoWidth=36)](https://lgtm.com/projects/g/kzonix/kzonix/alerts/)
[![DeepScan grade](https://deepscan.io/api/teams/4710/projects/6457/branches/53848/badge/grade.svg)](https://deepscan.io/dashboard#view=project&tid=4710&pid=6457&bid=53848)

[![Dependabot Status](https://api.dependabot.com/badges/status?host=github&repo=kzonix/kzonix)](https://dependabot.com)


[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=kzonix_kzonix&metric=code_smells)](https://sonarcloud.io/dashboard?id=kzonix_kzonix)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=kzonix_kzonix&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=kzonix_kzonix)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=kzonix_kzonix&metric=ncloc)](https://sonarcloud.io/dashboard?id=kzonix_kzonix)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=kzonix_kzonix&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=kzonix_kzonix)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=kzonix_kzonix&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=kzonix_kzonix)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=kzonix_kzonix&metric=security_rating)](https://sonarcloud.io/dashboard?id=kzonix_kzonix)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=kzonix_kzonix&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=kzonix_kzonix) 


[![Codacy Badge](https://api.codacy.com/project/badge/Grade/985d9f9265894c8aa955000dee18d155)](https://www.codacy.com/app/limpid-kzonix/kzonix?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=kzonix/kzonix&amp;utm_campaign=Badge_Grade) [![Greenkeeper badge](https://badges.greenkeeper.io/kzonix/kzonix.svg)](https://greenkeeper.io/)
